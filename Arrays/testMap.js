const { map } = require("./arrays.js");
const items = [1, 2, 3, 4, 5, 5];

console.log(map(items, (x) => {
  if (x > 3) return [x];
}))