const { find } = require("./arrays.js");
const items = [1, 2, 3, 4, 5, 5];

console.log(find(items, (x) => {
    if (x >= 4) return true;
}))