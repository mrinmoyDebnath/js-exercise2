function each(elements, cb) {
  // Do NOT use forEach to complete this function.
  // Iterates over a list of elements, yielding each in turn to the `cb` function.
  // This only needs to work with arrays.
  // You should also pass the index into `cb` as the second argument
  // based off http://underscorejs.org/#each

  for (let i = 0; i < elements.length; i++) {
    cb(elements[i], i, elements);
  }
}
function filter(elements, cb) {
  // Do NOT use .filter, to complete this function.
  // Similar to `find` but you will return an array of all elements that passed the truth test
  // Return an empty array if no elements pass the truth test

  let result = [];
  for (let i = 0; i < elements.length; i++) {
    if (cb(elements[i])) {
      result.push(elements[i]);
    }
  }
  return result;
}

function find(elements, cb) {
  // Do NOT use .includes, to complete this function.
  // Look through each value in `elements` and pass each element to `cb`.
  // If `cb` returns `true` then return that element.
  // Return `undefined` if no elements pass the truth test.
  for (let i = 0; i < elements.length; i++) {
    if (cb(elements[i])) {
      return elements[i];
    }
  }
  return undefined;
}

function map(elements, cb) {
  // Do NOT use .map, to complete this function.
  // How map works: Map calls a provided callback function once for each element in an array, in order, and function creates a new array from the res .
  // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
  // Return the new array.
  let result = [];
  let x;
  for (let i = 0; i < elements.length; i++) {
    x = cb(elements[i]);
    // console.log(x)
    if (x) result = result.concat(x);
  }
  return result;
}

function flatten(arr) {
  return arr.reduce((acc, val) => {
    if (val instanceof Array) return acc.concat(flatten(val));
    else return acc.concat(val);
  }, []);
}

module.exports = { each, filter, find, map, flatten };
