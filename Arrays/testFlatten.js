const { flatten } = require("./arrays.js");
const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

console.log(flatten(nestedArray));