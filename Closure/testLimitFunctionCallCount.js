const { limitFunctionCallCount } = require('./closures');


let lfc = limitFunctionCallCount(() => console.log('hello'), 3);
lfc();
lfc();
lfc();
lfc();
lfc();