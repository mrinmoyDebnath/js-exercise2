const { cacheFunction } = require('./closures')

let cache = cacheFunction(() => Date.now());
console.log(cache('what', 1));
console.log(cache('this', 23));
console.log(cache('what', 2));
console.log(cache('this', 23));