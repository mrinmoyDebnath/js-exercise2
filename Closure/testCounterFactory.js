const { counterFactory } = require('./closures');

let ob = counterFactory();
console.log(ob.increment());
console.log(ob.increment());
console.log(ob.decrement());