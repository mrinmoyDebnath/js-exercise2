const testObject = { name: 'Bruce Wayne', age: '36', location: 'Gotham' };

const { mapObject } = require('./object.js');

console.log(mapObject(testObject, (x) => {
    if (x === 'Gotham') return 'I am batman';
}))