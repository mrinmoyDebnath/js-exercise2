function keys(obj) {
	// Retrieve all the names of the object's properties.
	// Return the keys as strings in an array.
	// Based on http://underscorejs.org/#keys

	const y = JSON.stringify(obj);
	let keys = [];
	let key = "";
	let fl = false;
	for (let i = y.length-1; i >=0 ; i--) {
		if (y[i] === ":") {
			fl = true;
		}
		if (y[i] === ","|| y[i] === '{'){
			fl = false;
			key = key.replace(/[":,}]/g, "");
			keys.push(key.split("").reverse().join(""));
			key = "";
		}
		if (fl) {
			key += y[i];
		}
	}
	keys = keys.reverse();
	return keys;

}
function values(obj) {
	// Return all of the values of the object's own properties.
	// Ignore functions
	// http://underscorejs.org/#values

	// const y = JSON.stringify(obj);
	// let values = [];
	// let key = "";
	// let fl = false;
	// for (let i = y.length-1; i >=0 ; i--) {
	// 	if (y[i] === ":") {
	// 		fl = true;
	// 	}
	// 	if (y[i] === ","|| y[i] === '{'){
	// 		fl = false;
	// 		key = key.replace(/[":,}]/g, "");
	// 		key = key.split("").reverse().join("");
	// 		if(key) values.push(obj[key]);
	// 		key = "";
	// 	}
	// 	if (fl) {
	// 		key += y[i];
	// 	}
	// }
	// values = values.reverse();
	// return values;
	
	
	
	
	
	function nestedValues(obj, res){
		for(prop in obj){
			// console.log('object: ', obj)
			// for( key in obj){
				if(typeof(obj[prop])=='object'){
					// res = res.push(obj[prop])
					res.push(obj[prop])
					nestedValues(obj[prop], res)
				}else{
					// console.log(obj[prop])
					res.push(obj[prop])
					// return res
				}
			}
		}
	result = []
	nestedValues(obj,result)
	return result
}
function mapObject(obj, cb) {
	// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
	// http://underscorejs.org/#mapObject
	
	const y = JSON.stringify(obj);
	let keys = [];
	let key = "";
	let fl = false;
	for (let i = y.length-1; i >=0 ; i--) {
		if (y[i] === ":") {
			fl = true;
		}
		if (y[i] === ","|| y[i] === '{'){
			fl = false;
			key = key.replace(/[":,}]/g, "");
			keys.push(key.split("").reverse().join(""));
			key = "";
		}
		if (fl) {
			key += y[i];
		}
	}
	
	const allKeys = keys.reverse();
	let result = {}
	for (let i = 0; i < allKeys.length; i++) {
		let val = cb(obj[allKeys[i]]);
		if (val) result[allKeys[i]] = val;
		else result[allKeys[i]] = obj[allKeys[i]];
	}
	return result;
}

function pairs(obj) {
	// Convert an object into a list of [key, value] pairs.
	// http://underscorejs.org/#pairs

	const y = JSON.stringify(obj);
	let keys = [];
	let key = "";
	let fl = false;
	for (let i = y.length-1; i >=0 ; i--) {
		if (y[i] === ":") {
			fl = true;
		}
		if (y[i] === ","|| y[i] === '{'){
			fl = false;
			key = key.replace(/[":,}]/g, "");
			keys.push(key.split("").reverse().join(""));
			key = "";
		}
		if (fl) {
			key += y[i];
		}
	}
	
	const allKeys = keys.reverse();
	let result = [];
	let val;
	for (let i = 0; i < allKeys.length; i++) {
		key = allKeys[i];
		val = obj[key];
		// console.log(val)
		result.push([key, val]);
	}
	return result;
}

function invert(obj){
	// Returns a copy of the object where the keys have become the values and the values the keys.
	// Assume that all of the object's values will be unique and string serializable.
	// http://underscorejs.org/#invert

	const y = JSON.stringify(obj);
	let result = {};
	let key = "";
	let fl = false;
	for (let i = y.length-1; i >=0 ; i--) {
		if (y[i] === ":") {
			fl = true;
		}
		if (y[i] === ","|| y[i] === '{'){
			fl = false;
			key = key.replace(/[":,}]/g, "");
			key = key.split("").reverse().join("");
			val = obj[key];
			result[val] = key;
			key = "";
		}
		if (fl) {
			key += y[i];
		}
	}
	return result;
}

function defaults(obj, defaultProps) {
	// Fill in undefined properties that match properties on the `defaultProps` parameter object.
	// Return `obj`.
	// http://underscorejs.org/#defaults

	argsKeys = Object.keys(obj);
	defaultPropsKeys = Object.keys(defaultProps);
	result = obj;
	for(let i = 0; i< defaultPropsKeys.length; i++){
		if(!argsKeys.includes(defaultPropsKeys[i])){
			result[defaultPropsKeys[i]] = defaultProps[defaultPropsKeys[i]];
		}
	}
	return result;

}

module.exports = { keys, values, mapObject, pairs, invert, defaults };
