const testObject = { name: 'Bruce Wayne', age: '36', location: 'Gotham' };

const {pairs} = require('./object.js');

console.log(pairs(testObject, (x) => {
    if (x === 'Gotham') return 'I am batman';
}))